#!/usr/bin/python
#Author:Bill Turczyn
#Date:2006/03/22
#Desc: Python implementation of single perceptron

import getopt,sys
from types import NoneType
import re


number = re.compile('^\d+$')
max_iterations=50000
data = [] #list to hold  data to classify
test_data = []
test_answers = []
answers=[]
weights = []
bias = 1
test_cases = None #number of test cases
attributes = None #number of attributes
current_iterations = 1
interactive = False
filename = None

#########################################################
#Function that  prints out help for the program         # 
#Author: Bill Turczyn                                   # 
#March 2006                                             #
#Language:Python 2.4                                    #
#########################################################
def usage():
    print """perceptron: Two ways that the program can be run. 
	    1. Interactive: Prompts user for all input
	    2. File input: File format <attribute> <space> <attribute> <space> <answer> ex:1 1 1
			   You can specify more than one attribute ex: 1 2 5 1 (between)  
    
    perceptron.py --help --interactive --file  --test_cases --attributes --max
    -h, --help  Displays help
    -i, --interactive  Runs program in interactive mode
    -f, --file  filename from which to read data from
    -a, --attributes  Set how many attributes per test case (Use for non-interactive mode)
    -m, --max   Overides the default max iterations Default: (Test Cases * Attributes * 50000)

example usage:
Run program in interactive mode.
\tpython perceptron.py --interactive
\tpython perceptron.py -i     


Runs program in non-interactive mode reading data from boolean_and.txt 
\tpython perceptron.py --attributes=2 --file=boolean_and.txt
\tpython perceptron.py -a 2 -f boolean_and.txt     

"""

#########################################################
#Function to collect arguments from user                # 
#Author: Bill Turczyn                                   # 
#March 2006                                             #
#Language:Python 2.4                                    #
#########################################################
def main():
    global filename,interactive,max_iterations,test_cases,attributes,test_filename
    interactive = False
    try:
        opts,args = getopt.getopt(sys.argv[1:],"hif:a:m:", ["help", "interactive","test_cases=", \
							       "file=", "attributes=","max="])
    except getopt.GetoptError,e:
	print e
        usage()
        sys.exit(2)
    if len(opts) == 0:
        usage()
        sys.exit()
	interactive = True
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()

        if o in ("-i", "--interactive"):
	    interactive = True

	if o in ("-f","--file"):
            filename = a
	
#	if o in ("-t","--test_file"):
#            test_filename = a

	if o in ("-a","--attributes"):
            attributes = int(a)

	if o in ("-m","--max"):
	    max_iterations = int(a)


def weighted_sum(inputs,weights,bias):
    return sum([xi*wi for xi,wi in zip(inputs,weights)]) + bias 

def collect_info():
    global attributes,test_cases,answers,weights,max_iterations
    if number.search(str(attributes)) and number.search(str(test_cases)):
	return
    while not number.search(str(attributes)) or not number.search(str(test_cases)):
	if not number.search(str(attributes)):
	    print "Please enter the number of attributes:",
	    attributes=raw_input()
	if not number.search(str(test_cases)):
	    print "Please enter the number of training cases:",
	    test_cases=raw_input()
    test_cases = int(test_cases)
    attributes = int(attributes)
    weights = [1 for x in range(attributes)]
    max_iterations = test_cases * attributes * max_iterations

def read_training_cases():
    global data, answers
    for test_case in range(1,test_cases+1):
	tc = []
	for attribute in range(1,attributes+1):
	    print "Enter Test Case: %d Attribute: %d\n>" % (test_case,attribute),
	    input = raw_input()
	    tc.append(int(input))
	data.append(tc)
	print "Enter Test Case: %d Answer(-1,1):\n>" % (test_case),
	input = raw_input()
	answers.append(int(input))

def read_test_cases():
    global attributes,test_data,test_answers
    print "Please enter the number of test cases:",
    test_cases=raw_input()
    test_cases = int(test_cases)
    for test_case in range(1,test_cases+1):
	tc = []
	for attribute in range(1,attributes+1):
	    print "Enter Test Case: %d Attribute: %d\n>" % (test_case,attribute),
	    input = raw_input()
	    tc.append(int(input))
	test_data.append(tc)
	print "Enter Test Case: %d Answer(-1,1):\n>" % (test_case),
	input = raw_input()
	test_answers.append(int(input))

def run_test_cases():
    global attributes,test_data,test_answers,weights,bias
    correctly_classified = 0
    incorrectly_classified = 0
    for case in range(len(test_data)):
	total = 0
	print "\t Test case: %d" % (case),
	total = weighted_sum(test_data[case],weights, bias)
	if total > 0 and test_answers[case] >= 0:
	    print "Classified %s Weighted Sum: %d Expected: %d" % (str(test_data[case]),total,test_answers[case]) 
	    correctly_classified += 1
	if total <= 0 and test_answers[case] < 0:
	    print "Classified %s Weighted Sum: %d Expected: %d" % (str(test_data[case]),total,test_answers[case]) 
	    correctly_classified += 1
	if total > 0 and test_answers[case] < 0:
	    print "Misclassified %s Weighted Sum: %d Expected: %d" % (str(test_data[case]),total,test_answers[case]) 
	    incorrectly_classified += 1
	if total <= 0 and test_answers[case] > 0:
	    print "Misclassified %s Weighted Sum: %d Expected: %d" % (str(test_data[case]),total,test_answers[case]) 
	    incorrectly_classified += 1
	print "\n"
    print "Total Cases: %-2d Correct: %-2.1f%% Incorrect:%-2.1f%%" % \
    (len(test_data),(float(correctly_classified)/len(test_data))*100,(float(incorrectly_classified)/len(test_data))*100)

def _test_cases():
    print "Would you like to enter some cases to test weights?[Y,N]\n>",
    an = raw_input()
    if an.upper() == 'N': return
    elif an.upper() == 'Y':
	read_test_cases()
	run_test_cases()



def read_data():
    global data,weights,attributes
    lines = 0 
    try:
	input = open(filename,'r')
	#set the number of attributes
	line = input.readline()
	line=[int(x) for x in line.split()]
	attributes = len(line)-1
	line = input.seek(0)
	while 1:
	    line = input.readline()
	    if not line:
		break
	    line=[int(x) for x in line.split()]
	    data.append(line[0:-1])
	    answers.append(line[-1])
	    lines += 1
	weights = [1 for x in range(attributes)]
    except IOError,e:
	print e

def classify_data():	
    global current_iterations,bias,data,answers,weights
    total_changes = 1
    while current_iterations <= max_iterations and total_changes > 0:
	total_changes = 0
	print "CURRENT ITERATION: %-6d\n" % (current_iterations)
	for case in range(len(data)):
	    total = 0
	    print "\t Test case: %-4d" % (case),
	    total = weighted_sum(data[case],weights, bias)

	    if total > 0 and answers[case] <= 0:
		total_changes += 1
		print "updating weights", 
		for input in range(len(data[case])):
		    if data[case][input]  >= 0:
			weights[input] = (weights[input] - data[case][input])
		    else:
			weights[input] = (weights[input] + data[case][input])
		bias = bias - 1

	    if total <= 0 and answers[case] > 0:
		total_changes += 1
		print "updating weights", 
		for input in range(len(data[case])):
		    if data[case][input]  >= 0:
			weights[input]= (weights[input]+ data[case][input])
		    else:
			weights[input]= (weights[input]- data[case][input])
		bias = bias + 1
	    print '\n'
	print "Total Missclassifications:%d" % (total_changes)
	print "-" * 50 
	current_iterations += 1
    print "Weights %s Bias: %d" % (str(weights), bias)

if __name__ == "__main__":
    main() 
    if interactive:
	collect_info()
	read_training_cases() 
	classify_data()
	if current_iterations <> max_iterations:
	    _test_cases()
	else:
	    print "Max iterations reached!!!"
    elif not interactive and type(filename) <> NoneType:#read data from file
	read_data()
	classify_data()
	if current_iterations <> max_iterations:
	    _test_cases()
	else:
	    print "Max iterations reached!!!"
	    
    else:
	usage()
