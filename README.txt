perceptron: Two ways that the program can be run. 
	    1. Interactive: Prompts user for all input
	    2. File input: File format <attribute> <space> <attribute> <space>
<answer> ex:1 1 1
			   You can specify more than one attribute ex: 1 2 5 1
(between)  
    
    perceptron.py --help --interactive --file  --test_cases --attributes --max
    -h, --help  Displays help
    -i, --interactive  Runs program in interactive mode
    -f, --file  filename from which to read data from
    -a, --attributes  Set how many attributes per test case (Use for
non-interactive mode)
    -m, --max   Overides the default max iterations Default: (Test Cases *
Attributes * 50000)

example usage:
Run program in interactive mode.
	python perceptron.py --interactive
	python perceptron.py -i     


Runs program in non-interactive mode reading data from boolean_and.txt 
	python perceptron.py --attributes=2 --file=boolean_and.txt
	python perceptron.py -a 2 -f boolean_and.txt     

